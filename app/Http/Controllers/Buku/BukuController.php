<?php

namespace App\Http\Controllers\Buku;

use App\Http\Controllers\Controller;
use App\Http\Requests\BukuRequest;
use App\Http\Resources\Buku as ResourcesBuku;
use App\Http\Resources\BukuCollection;
use App\Models\Buku;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BukuController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth:api', 'role:ADMIN'])->except(['index', 'show']);
        // $this->middleware('role:ADMIN')->except(['index', 'shosw']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new BukuCollection([Buku::get()]);
    }

    protected function bookStore()
    {
        return [
            'kode_buku' => request('kode_buku'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit')
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        Buku::create($this->bookStore());

        return 'Buku Uploaded';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        return new ResourcesBuku($buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BukuRequest $request, Buku $buku)
    {
        $buku->update($this->bookStore());

        return 'Buku Updated';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        $buku->delete();

        return 'Buku Deleted';
    }
}
