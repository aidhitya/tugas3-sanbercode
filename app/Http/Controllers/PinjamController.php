<?php

namespace App\Http\Controllers;

use App\Http\Requests\PinjamRequest;
use App\Http\Resources\PinjamCollection;
use App\Http\Resources\PinjamResource;
use App\Models\Pinjaman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PinjamController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->middleware('role:ADMIN')->except(['store', 'show', 'semuaKhusus']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new PinjamCollection(Pinjaman::get());
    }

    public function semuaKhusus()
    {
        // return Auth::user()->id;
        return new PinjamCollection(Pinjaman::where('user_id', auth()->user()->id)->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PinjamRequest $request)
    {
        Auth::user()->meminjam()->create([
            'kode_buku' => request('kode_buku'),
            'mulai_pinjam' => request('mulai_pinjam'),
            'akhir_pinjam' => request('akhir_pinjam')
        ]);

        return 'Pinjaman Accepted';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new PinjamResource(Pinjaman::where('user_id', auth()->user()->id)->where('id', $id)->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_pengembalian' => ['required', 'after_or_equal:akhir_pinjam']
        ]);

        $pinjam = Pinjaman::findOrFail($id);
        $status = 1;
        $d = strtotime($pinjam->akhir_pinjam);
        $s = strtotime(request('tanggal_pengembalian'));

        if ($d < $s) {
            $status = 0;
        }

        $pinjam->update([
            'tanggal_pengembalian' => request('tanggal_pengembalian'),
            'ontime' => $status
        ]);

        return 'Returned';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Pinjaman::destroy($id);
    }
}
