<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BukuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_buku' => ['required', 'unique:bukus,kode_buku'],
            'judul' => ['required', 'string'],
            'pengarang' => ['required', 'string'],
            'tahun_terbit' => ['required', 'integer']
        ];
    }
}
