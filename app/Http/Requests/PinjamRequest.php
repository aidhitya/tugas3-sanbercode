<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PinjamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'kode_buku' => ['sometimes', 'required', 'exists:bukus,kode_buku'],
            'mulai_pinjam' => ['sometimes', 'required', 'date'],
            'akhir_pinjam' => ['sometimes', 'required', 'date', 'after:mulai_pinjam'],
            'tanggal_pengembalian' => ['sometimes', 'required', 'after_or_equal:akhir_pinjam']
        ];
    }
}
