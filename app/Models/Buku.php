<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pinjaman;

class Buku extends Model
{
    protected $guarded = [];

    public function getRouteKeyName()
    {
        return 'kode_buku';
    }

    public function pinjam()
    {
        return $this->hasMany(Pinjaman::class);
    }
}
